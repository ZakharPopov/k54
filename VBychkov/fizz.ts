let a: Array<number | string> = ['60', '12 3', 13, 61, 64, 33, 18, 3, 35, 77, 16, 25, '21', 63, 20, 30, 65, 94, 68, 67, 37, 97]

const replaceNumberToFizzBuzz: Function = (array: Array<number | string>) :Array<number|string> => {
    array.map((value: number | string, index: number) => {
        if (+value % 3 === 0 && +value % 5 === 0) {
            a[index] = 'FizzBuzz'
        } else if (+value % 3 === 0) {
            a[index] = "Fizz"
        } else if (+value % 5 === 0) {
            a[index] = 'Buzz'
        }

    })
    return array
}

console.log(replaceNumberToFizzBuzz(a))