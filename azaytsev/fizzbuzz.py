def fizzbuzz (args):
    res_list = []
    for i in args:
        res = ""
        if i % 3 == 0:
            res += "Fizz"
        if i % 5 == 0:
            res += "Buzz"
        if res == "":
            res_list.append(i)
        else:
            res_list.append(res)
    print(res_list)