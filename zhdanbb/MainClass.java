package zhdanbb;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainClass {

    public static void main(String[] args) {
        int[] setValue = new int[10];
        Random random = new Random();
        for (int i = 0; i < setValue.length; i++){
            setValue[i] = random.nextInt(100);
        }
        setValue[9] = 15;
        ArrayList getValue = FizzBuzz(setValue);
        getValue.forEach(System.out::println);
    }
    public static ArrayList FizzBuzz(int[] chain){
        //Получаем int`овый массив, возвращаем простой список
        ArrayList mainList = new ArrayList<>();

        for (int i : chain){
            if(i % 3 == 0 && i % 5 == 0){
                mainList.add("FizzBuzz");
            }
            else if (i % 5 == 0){
                mainList.add("Buzz");
            }
            else if (i % 3 == 0){
                mainList.add("Fizz");
            }
            else{
                mainList.add(i);
            }
        }

        return mainList;
    }
}
