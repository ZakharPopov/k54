function FizzBuzzFunc(argList){
  var sampleList = []
  for(i = 0; i < argList.length; i++){
    var listElement = argList[i]

    if(listElement % 5 === 0 && listElement % 3 === 0){
      sampleList.push('FizzBuzz')
      continue
    }

    if(listElement % 3 === 0){
      sampleList.push('Fizz')
      continue
    }

    if(listElement % 5 === 0){
      sampleList.push('Buzz')
      continue
    }

    sampleList.push(listElement)
  }
  return sampleList
}

console.log(FizzBuzzFunc([3,5,3,15]))